#include <iostream>
#include <Cvrptw.h>
#include <exceptions/Exception.h>
#include <fstream>

int main(int argc, char* argv[]) {
    Cvrptw cvrptw = Cvrptw(argv[1]);
    std::cout << "Wczytano " << cvrptw.nodes.size() << "wierzchołków" << std::endl;

    try{
        Solution *solution = cvrptw.run();
        solution->write_to_file("output.txt");
    }catch(NotPossible &e){
        std::ofstream outfile;
        outfile.open("output.txt", std::ios::out | std::ios::trunc );
        outfile << "-1";
        outfile.close();
    }

    return 0;
}