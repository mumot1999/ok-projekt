import glob
import subprocess

files = [f for f in glob.glob("./**", recursive=True)]
files = list(filter(lambda x: '.txt' in x.lower() and 'output' not in x, files))
passed = []
for file in files:
    subprocess.getoutput('./../cmake-build-debug/Project_run {}'.format(file))
    response = subprocess.getoutput('wine ./ckrptw.exe {} {}'.format(file, './output.txt'))
    print(file + "->" + response)
    passed.append('ok' in response.lower())


print("Status: " + str(all(passed)))
