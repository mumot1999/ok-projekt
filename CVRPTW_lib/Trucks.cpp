//
// Created by bartek on 27.10.19.
//

#include "Trucks.h"

Trucks::Trucks(int trucks, int capacity) {
    this->add_trucks(trucks, capacity);
}

void Trucks::add_trucks(int trucks_count, int capacity) {
    for (int i = 0; i < trucks_count; ++i) {
        Truck truck = Truck(i, capacity);
        this->trucks.push_back(truck);
    }
}

Trucks::Trucks() = default;
