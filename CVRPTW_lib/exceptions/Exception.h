//
// Created by bartlomiej on 18.11.2019.
//

#ifndef TESTING_TEST_EXCEPTION_H
#define TESTING_TEST_EXCEPTION_H

#include <exception>
#include <stdexcept>

class VisitNodeException : public std::exception {};

class NotPossible : public VisitNodeException {};
class NodeAlreadyVisited : public VisitNodeException {};
class TooMuchCargo : public VisitNodeException {};
class NodeClosed : public VisitNodeException {};
class BackToDepotWillBeNotPossible : public VisitNodeException {};


#endif //TESTING_TEST_EXCEPTION_H
