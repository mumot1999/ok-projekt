//
// Created by bartek on 27.10.19.
//

#ifndef TESTING_TEST_CVRPTW_H
#define TESTING_TEST_CVRPTW_H

#include <Solution/Solution.h>
#include "Trucks.h"
#include "node/Nodes.h"

class Cvrptw {
public:

    Nodes nodes;
    Trucks trucks;

    Cvrptw(std::string path);
    Cvrptw(Nodes nodes, Trucks trucks);


    bool is_possible();
    Solution * run();
    void set_trucks_location();

};


#endif //TESTING_TEST_CVRPTW_H
