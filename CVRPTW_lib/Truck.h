//
// Created by bartek on 27.10.19.
//

#ifndef TESTING_TEST_TRUCK_H
#define TESTING_TEST_TRUCK_H

#include <vector>
#include <Solution/Route.h>

class Truck {
public:
    int id;
    int capacity;
    double actual_time;
    Node* location;

    std::vector< Route* > routes;

    Truck(int id, int capacity);


};


#endif //TESTING_TEST_TRUCK_H
