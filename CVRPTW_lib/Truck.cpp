//
// Created by bartek on 27.10.19.
//

#include <node/Node.h>
#include "Truck.h"
#include <stdexcept>

Truck::Truck(int id, int capacity) {
    this->id=id;
    this->capacity=capacity;
    this->location = nullptr;
    this->actual_time = 0;
}
