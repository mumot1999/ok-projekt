//
// Created by bartek on 27.10.19.
//


#include "Nodes.h"

Nodes::Nodes(){
    this->nodes = new std::vector< Node* >;
};


Nodes::Nodes(int nodes) {
    this->nodes = new std::vector< Node* >;

    for (int i = 0; i < nodes; ++i) {
        this->push_node(new Node(i));
    }
}

Node *Nodes::operator[](int i) {
    return this->get_node(i);
}


Node* Nodes::first(){
    if(this->size() > 0){
        return this->nodes->at(0);
    }
    else{
        return nullptr;
    }
}

Node* Nodes::get_node(int i) {
    return this->nodes->at(i);
}

Nodes *Nodes::filter_nodes(std::function<bool(Node*)> pFunction) {
    auto* new_nodes = new Nodes();
    std::copy_if(this->nodes->begin(),
                 this->nodes->end(),
                 std::back_inserter(*(new_nodes->nodes)),
                 std::move(pFunction));
    return new_nodes;
}

Nodes *Nodes::sort_nodes(std::function<bool(Node*, Node*)> pFunction) {
    std::sort(nodes->begin(), nodes->end(), std::move(pFunction));
    return this;
}


Nodes* Nodes::get_open_nodes(Node* actual_node, double time = 0) {
    return this->filter_nodes([&actual_node, &time](Node *node) {
        return actual_node->distance_to_node(node) <= node->time_start + time;
    });
}

Nodes * Nodes::get_unvisited_nodes(){
    return this->filter_nodes([](Node *node) {
        return !(node->visited || node->id == 0);
    });
}

Nodes* Nodes::sort_by_distance(Node* node) {
    return this->filter_nodes([node](Node* node1){
        return !(node1->id == node->id || node1->id == 0);
    })->sort_nodes([node](Node* a, Node* b) {return a->distance_to_node(node) < b->distance_to_node(node); });
}

Nodes * Nodes::sort_by_time_end() {
    return this->filter_nodes([](Node* node){
        return node->id != 0;
    })->sort_nodes([](Node* a, Node* b) {
        return a->time_end < b->time_end;
    });
}

void Nodes::push_node(Node* node){
    this->nodes->push_back(node);
}

int Nodes::size() {
    return (int) this->nodes->size();
}

void Nodes::calculate_in_out(unsigned long limit) {
    for(unsigned long i = 0; i<nodes->size(); i++){
        Node* node = this->get_node(i);
        Nodes *pNodes = this->sort_by_distance(node);
        auto nodes_in = pNodes->filter_nodes([node](Node* node1){
            if(node1->id == 0)
                return false;
            double distance = node1->distance_to_node(node);
            return (node1->time_start + node1->unload_duration + distance) <= node->time_end;
        });
        auto nodes_out = pNodes->filter_nodes([node](Node* node1){
            if(node1->id == 0)
                return false;
            double distance = node1->distance_to_node(node);
            return (node->time_start + node->unload_duration + distance) <= node1->time_end;
        });

        node->in = nodes_in->nodes;
        node->out = nodes_out->nodes;
    }
}

Nodes *Nodes::limit(unsigned long limit) {
    auto* new_nodes = new Nodes();
    std::copy(this->nodes->begin(),
                 this->nodes->end(),
                 std::back_inserter(*(new_nodes->nodes)));
    new_nodes->nodes->resize(limit < new_nodes->size() ? limit : new_nodes->size());
    return new_nodes;
}

bool Nodes::has_node(Node *node) {
    for(auto* visited_node : *nodes){
        if(visited_node->id == node->id)
            return true;
    }
    return false;
}

Nodes *Nodes::no_depot() {
    return this->filter_nodes([](Node* node){
        return node->id > 0;
    });
}

int Nodes::cargo() {
    int cargo = 0;
    for(auto* node : *this->nodes){
        cargo += node->cargo;
    };
    return cargo;
}

std::list<int> Nodes::nodes_list() {
    std::list <int> ids;
    for(auto* node : *nodes){
        ids.push_back(node->id);
    }
    return ids;
}

Nodes::Nodes(std::vector<Node *> *pVector) {
    this->nodes = pVector;
}


