//
// Created by bartek on 27.10.19.
//

#ifndef TESTING_TEST_NODES_H
#define TESTING_TEST_NODES_H


#include <algorithm>
#include <functional>
#include <utility>
#include <vector>
#include <list>
#include "Node.h"

class Nodes {
public:
    Nodes(std::vector<Node *> *pVector);

    std::vector< Node* >* nodes;

    Nodes();
    explicit Nodes(int);

    Node * operator[] (int);

    Node* first();
    Node* get_node(int i);

    Nodes *filter_nodes(std::function<bool(Node *)> pFunction);
    Nodes *sort_nodes(std::function<bool(Node *, Node *)> pFunction);

    Nodes* get_open_nodes(Node *actual_node, double time);
    Nodes* get_unvisited_nodes();
    Nodes* sort_by_distance(Node* node);
    Nodes* sort_by_time_end();

    Nodes* limit(unsigned long limit);

    void calculate_in_out(unsigned long limit);
    void push_node(Node *node);
    int size();

    bool has_node(Node *node);

    Nodes *no_depot();
    std::list <int> nodes_list();
    int cargo();
};


#endif //TESTING_TEST_NODES_H
