//
// Created by bartek on 27.10.19.
//

#include <node/Nodes.h>
#include <sstream>
#include <fstream>
#include <Solution/Solution.h>
#include <exceptions/Exception.h>
#include "Cvrptw.h"
#include "Trucks.h"
#include "stdexcept"

Cvrptw::Cvrptw(Nodes nodes, Trucks trucks) {
    this->nodes=nodes;
    this->trucks=trucks;
    this->set_trucks_location();
}

Solution * Cvrptw::run() {
    if (!this->is_possible())
        throw NotPossible();
    Nodes *unvisited_nodes = nodes.get_unvisited_nodes()->no_depot();
    auto* solution = new Solution(nodes.get_node(0), this->trucks.trucks[0].capacity);

    while(unvisited_nodes->size()){
        Route *new_route = solution->create_new_route();
        for(auto node : *unvisited_nodes->nodes){
            try{
                new_route->visit_node(node);
            }catch (VisitNodeException &e){

            }
        }
        unvisited_nodes = nodes.get_unvisited_nodes()->no_depot();
    }
    return solution;
}

bool Cvrptw::is_possible() {
    Node* depot = this->nodes.get_node(0);

    if(depot->time_end){
        for(int i = 1; i < this->nodes.size(); i++){
            Node* node = this->nodes[i];

            if(node->time_start + depot->distance_to_node(node) >= depot->time_end)
            {
                return false;
            }
        }
    }

    return true;
}

void Cvrptw::set_trucks_location() {
    for(auto & truck : this->trucks.trucks){
        truck.location = this->nodes[0];
    }
}

std::vector<std::string> explode(std::string const & s, char delim)
{
    std::vector<std::string> result;
    std::istringstream iss(s);

    for (std::string token; std::getline(iss, token, delim); )
    {
        token.erase(token.find_last_not_of("\n\r")+1);
        if(token.length())
            result.push_back(std::move(token));
    }

    return result;
}

Cvrptw::Cvrptw(std::string path) {
    this->trucks = Trucks();
    std::ifstream infile(path);

    std::string line;
    int x= 0;
    Trucks trucks = Trucks();
    Nodes nodes = Nodes();
    char arr[] = { ' ', '\t' };

    while (std::getline(infile, line))
    {

        for(char delimiter : arr){
            auto v = explode(line, delimiter);
            try{
                if(v.size() >=2){
                    std::vector <int> data;
                    for(auto& n : v){
                        data.push_back(std::stoi(n));
                    }
                    if(data.size() == 2){
                        trucks.add_trucks(data[0], data[1]);
                    }else if(data.size() == 7){
                        nodes.push_node(new Node(data[0], data[1], data[2], data[3], data[4], data[5], data[6]));
                    };
                    break;
                }
            }catch(...){

            }
        }

    }
    this->nodes = nodes;
    this->trucks = trucks;
    this->set_trucks_location();
}

