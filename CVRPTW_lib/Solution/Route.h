//
// Created by bartek on 08.11.19.
//

#ifndef TESTING_TEST_ROUTE_H
#define TESTING_TEST_ROUTE_H

#include <node/Nodes.h>

class Route {
public:
    Route(Node* depot, int cargo);

    double actual_time;
    int cargo;

    Node *depot;
    Nodes* route;

    void visit_node(Node* node);

    Node *actual_position() const;

    std::vector<Node*> *get_route();
};


#endif //TESTING_TEST_ROUTE_H
