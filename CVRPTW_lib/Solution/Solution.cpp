//
// Created by bartek on 17.11.19.
//

#include <exceptions/Exception.h>
#include <fstream>
#include <iomanip>
#include "Solution.h"
#include "Route.h"

bool Solution::is_visited(Node *node) {
    for(auto* route : *routes){
        for(auto* visited_node : *route->get_route()){
            if(visited_node->id == node->id)
                return true;
        }
    }
    return false;
}

bool Solution::is_solution_good(int nodes) {
    Nodes visited_nodes;
    for (auto *route : *this->routes) {
        double actual_time = 0;
        Node* node_from = depot;

        for (auto* node_to : *route->get_route()) {
            if (visited_nodes.has_node(node_to)){
                return false;
            }
            double distance = node_from->distance_to_node(node_to);
            actual_time += distance;
            if(actual_time > node_to->time_end)
                return false;
            actual_time += node_to->unload_duration;

            visited_nodes.push_node(node_to);
            node_from = node_to;
        }


        double distance = node_from->distance_to_node(depot);
        actual_time += distance;
        if(actual_time > depot->time_end)
            return false;
    }
    if(nodes != visited_nodes.size())
        return false;

    return true;
}

Score Solution::get_score(){
    Score score = {0, 0};

    for (auto *route : *routes) {
        double actual_time = 0;

        Node* node_from = this->depot;

        for (auto* node_to : *route->get_route()) {
            double distance = node_from->distance_to_node(node_to);
            score.distance += distance;
            actual_time += distance;
            if(actual_time < node_to->time_start){
                actual_time = node_to->time_start;
            }
            actual_time += node_to->unload_duration;
            node_from = node_to;
        }

        double distance = node_from->distance_to_node(this->depot);
        actual_time += distance;

        score.distance += distance;
        score.time += actual_time;
    }

    return score;

}

Solution::Solution(Node *depot, int cargo) {
    this->depot = depot;
    this->cargo = cargo;
    this->routes = new std::vector<Route*>;
}

Nodes *Solution::get_unvisited_nodes(Nodes *pNodes) {
    return pNodes->filter_nodes([this](Node* node){
        return !this->is_visited(node);
    });
}

Route* Solution::create_new_route(){
    auto *route = new Route(this->depot, this->cargo);
    this->routes->push_back(route);
    return route;
}

void Solution::write_to_file(const char *file_name) {
    std::ofstream outfile;
    outfile.open(file_name, std::ios::out | std::ios::trunc );

    outfile << routes->size() << " ";
    outfile << std::fixed << std::setprecision(8) << get_score().time << std::endl;

    for(auto* route : *routes){
        for(auto* node : *route->get_route()){
            outfile << node->id << " ";
        }
        outfile << std::endl;
    }

    outfile.close();
}
