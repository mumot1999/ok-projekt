//
// Created by bartek on 08.11.19.
//

#include <node/Node.h>
#include <exceptions/Exception.h>
#include "Route.h"

Route::Route(Node* depot, int cargo) {
    this->actual_time = 0;
    this->cargo = cargo;

    this->depot = depot;

    this->route = new Nodes();
}


void Route::visit_node(Node *node) {
    if(node->visited){
        throw NodeAlreadyVisited();
    }
    if(this->cargo - node->cargo < 0){
        throw TooMuchCargo();
    }

    double distance_to_node = actual_position()->distance_to_node(node);

    double time_unloading = this->actual_time + distance_to_node;

    if(time_unloading > (double)node->time_end){
        throw NodeClosed();
    }

    if(time_unloading < node->time_start)
        time_unloading = node->time_start;

    double time_after_unloading = time_unloading + node->unload_duration;


    
    if(this->depot->time_end
    && (time_after_unloading + node->distance_to_node(this->depot) > this->depot->time_end))
        throw BackToDepotWillBeNotPossible();

    node->visited = true;

    this->cargo -= node->cargo;
    this->route->push_node(node);

    this->actual_time = time_after_unloading;
}

Node *Route::actual_position() const {
    if(this->route->size())
        return route->nodes->back();
    else
        return this->depot;
}

std::vector<Node*> *Route::get_route() {
    return this->route->nodes;
}
