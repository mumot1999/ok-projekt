//
// Created by bartek on 17.11.19.
//

#ifndef TESTING_TEST_SOLUTION_H
#define TESTING_TEST_SOLUTION_H


#include <vector>
#include <node/Nodes.h>
#include <list>
#include "Route.h"

struct Score {
    double time;
    double distance;
};

class Solution {
public:
    Node* depot;
    int cargo;

    std::vector< Route* >* routes;
    explicit Solution(Node *depot, int cargo);

    bool is_visited(Node* node);
    bool is_solution_good(int nodes);

    Score get_score();


    Nodes *get_unvisited_nodes(Nodes *pNodes);

    Route *create_new_route();

    void write_to_file(const char file_name[11]);
};


#endif //TESTING_TEST_SOLUTION_H
